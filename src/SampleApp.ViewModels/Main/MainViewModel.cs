﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Mvvm;
using System.Windows.Threading;

using SampleApp.Domain;
using SampleApp.Models;

namespace SampleApp.ViewModels.Main
{
	public class MainViewModel : ViewModelBase
	{
		private readonly INavigationService _navigationService;
		private readonly ApplicationModel _appModel;

		private bool _actionInProgress;
		private IPInformation _selectedObject;

		public MainViewModel(INavigationService navigationService, ApplicationModel appModel)
		{
			_navigationService = navigationService;
			_appModel = appModel;

			GetInformationCommand = new DelegateCommand(StartGetInformation);
		}

		public bool ActionInProgress
		{
			get { return _actionInProgress; }
			private set
			{
				if (_actionInProgress != value)
				{
					_actionInProgress = value;
					RaisePropertyChanged(nameof(ActionInProgress));
				}
			}
		}

		public IPInformation SelectedObject
		{
			get { return _selectedObject; }
			private set
			{
				_selectedObject = value;
				RaisePropertyChanged(nameof(SelectedObject));
			}
		}

		public ICommand GetInformationCommand { get; }

		protected override void OnViewLoaded(IView view)
		{
			base.OnViewLoaded(view);

			_appModel.IpInformationLoaded += OnIpInformationLoaded;
			_appModel.IpInformationFailed += OnIpInformationFailed;
		}

		protected override void OnViewUnloaded()
		{
			base.OnViewUnloaded();

			_appModel.IpInformationLoaded -= OnIpInformationLoaded;
			_appModel.IpInformationFailed -= OnIpInformationFailed;
		}

		private async void StartGetInformation()
		{
			ActionInProgress = true;
			SelectedObject = null;

			await TaskEx.Delay(2000); // emulate too long processing

			await _appModel.StartGetIpInformationAsync();
		}

		private void OnIpInformationLoaded(IPInformation information)
		{
			ActionInProgress = false;

			SelectedObject = information;
		}

		private void OnIpInformationFailed(Exception error, string serverMessage)
		{
			ActionInProgress = false;

			MessageBox.Show($"{error.Message}: {serverMessage}", "Exception occured");
		}
	}
}