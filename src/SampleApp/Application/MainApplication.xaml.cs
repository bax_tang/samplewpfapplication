﻿using System;
using System.Windows;
using System.Windows.Mvvm;
using System.Windows.Navigation;

using SampleApp.ViewModels.Main;

namespace SampleApp
{
	public partial class MainApplication : Application
	{
		private readonly INavigationService _navigationService;

		public MainApplication(INavigationService navigationService) : base()
		{
			InitializeComponent();

			_navigationService = navigationService;
		}

		protected override void OnNavigating(NavigatingCancelEventArgs e)
		{
			base.OnNavigating(e);
		}

		protected override void OnNavigated(NavigationEventArgs e)
		{
			base.OnNavigated(e);
		}

		protected override void OnActivated(EventArgs e)
		{
			base.OnActivated(e);
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			_navigationService.NavigateToViewModel<MainViewModel>();
		}
	}
}