﻿using System;
using System.Windows;
using System.Windows.Navigation;

using Microsoft.Practices.Unity;

namespace SampleApp
{
	internal class EntryPoint
	{
		[STAThread]
		internal static void Main(string[] args)
		{
			var mainWindow = new MainWindow();

			using (var container = ContainerFactory.Build(mainWindow.NavigationService))
			{
				container.Resolve<MainApplication>().Run(mainWindow);
			}
		}
	}
}