﻿using System;
using System.Windows;
using System.Windows.Navigation;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Wpf;

using SampleApp.Models;
using SampleApp.Services.Network;
using SampleApp.ViewModels.Main;
using SampleApp.Views.Main;

namespace SampleApp
{
	internal static class ContainerFactory
	{
		public static IUnityContainer Build(NavigationService navigationService)
		{
			var container = new UnityContainer();

			container.RegisterType<MainApplication>(InstanceLifetime.Single);

			container.RegisterNavigationService(navigationService);

			container.RegisterType<ResponseParser>(InstanceLifetime.Single);
			container.RegisterType<ServerGateway>(InstanceLifetime.Single);
			container.RegisterType<ApiService>(InstanceLifetime.Single);
			container.RegisterType<ApplicationModel>(InstanceLifetime.Single);

			container.Bind<MainViewModel, MainView>(viewModelLifetime: InstanceLifetime.Single, viewLifetime: InstanceLifetime.Single);

			return container;
		}
	}
}