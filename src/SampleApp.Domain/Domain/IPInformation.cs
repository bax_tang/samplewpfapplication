﻿using System;

using Newtonsoft.Json;

namespace SampleApp.Domain
{
	public class IPInformation
	{
		[JsonProperty("ip")]
		public string Address { get; set; }

		[JsonProperty("hostname")]
		public string Hostname { get; set; }

		[JsonProperty("country")]
		public string Country { get; set; }

		[JsonProperty("region")]
		public string Region { get; set; }

		[JsonProperty("city")]
		public string City { get; set; }

		[JsonProperty("loc")]
		public string Location { get; set; }

		[JsonProperty("org")]
		public string Organization { get; set; }
	}
}