﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SampleApp.Services.Network
{
	internal class RequestState
	{
		internal TaskCompletionSource<ServerResponseRaw> CompletionSource { get; private set; }

		internal HttpRequestMessage RequestMessage { get; private set; }

		internal CancellationToken Token { get; private set; }

		internal HttpWebRequest Request { get; set; }

		internal RequestState(TaskCompletionSource<ServerResponseRaw> completionSource, HttpRequestMessage requestMessage, CancellationToken cancellationToken)
		{
			CompletionSource = completionSource;
			RequestMessage = requestMessage;
			Token = cancellationToken;
		}
	}
}