﻿using System;
using System.IO;
using System.Net;

namespace SampleApp.Services.Network
{
	internal class ServerResponseRaw
	{
		public HttpStatusCode StatusCode { get; private set; }

		public Stream ResponseStream { get; private set; }

		public WebHeaderCollection Headers { get; private set; }

		internal ServerResponseRaw(HttpStatusCode statusCode, Stream responseStream, WebHeaderCollection headers)
		{
			StatusCode = statusCode;
			ResponseStream = responseStream;
			Headers = headers;
		}
	}
}