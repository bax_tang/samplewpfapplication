﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using SampleApp.Common;
using SampleApp.Domain;

namespace SampleApp.Services.Network
{
	public class ApiService
	{
		private readonly ServerGateway _serverGateway;
		private readonly ResponseParser _responseParser;
		
		public ApiService(ServerGateway serverGateway, ResponseParser responseParser)
		{
			_serverGateway = serverGateway;
			_responseParser = responseParser;
		}
		
		public async Task<ServerResponse<IPInformation>> GetIpInformationAsync()
		{
			return await PerformCommonGetRequest<IPInformation>(new Uri("https://ipinfo.io/json"));
		}
		
		private async Task<ServerResponse<TResult>> PerformCommonGetRequest<TResult>(Uri requestUri) where TResult : class
		{
			HttpStatusCode statusCode = 0;

			try
			{
				var response = await _serverGateway.PerformGetRequest(requestUri, default(CancellationToken));

				statusCode = response.StatusCode;
				return CreateServerResponse<TResult>(response);
			}
			catch (Exception exc)
			{
				return new ServerResponse<TResult>(exc, statusCode);
			}
		}

		private ServerResponse<TResult> CreateServerResponse<TResult>(ServerResponseRaw response) where TResult : class
		{
			int statusCode = (int)response.StatusCode;
			if (statusCode >= 200 && statusCode <= 299)
			{
				var result = _responseParser.ParseTo<TResult>(response.ResponseStream);
				return new ServerResponse<TResult>(result, response.StatusCode);
			}

			string serverMessage = _responseParser.ParseToString(response.ResponseStream);
			return new ServerResponse<TResult>(serverMessage, response.StatusCode);
		}
	}
}