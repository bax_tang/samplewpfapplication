﻿using System;
using System.Net;

namespace SampleApp.Services.Network
{
	public class ServerResponse<TResult> where TResult : class
	{
		public HttpStatusCode StatusCode { get; private set; }

		public TResult ResponseResult { get; private set; }

		public Exception Error { get; private set; }

		public string ServerMessage { get; private set; }

		public ServerResponse(TResult result, HttpStatusCode statusCode)
		{
			ResponseResult = result;
			StatusCode = statusCode;
		}

		public ServerResponse(Exception error, HttpStatusCode statusCode)
		{
			Error = error;
			StatusCode = statusCode;
		}

		public ServerResponse(string serverMessage, HttpStatusCode statusCode)
		{
			ServerMessage = serverMessage;
			StatusCode = statusCode;
		}

		public bool IsSuccess()
		{
			int statusCode = (int)StatusCode;

			return ((statusCode >= 200) && (statusCode <= 299)) && (ResponseResult != null);
		}
	}
}