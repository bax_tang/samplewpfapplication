﻿using System;
using System.IO;
using System.Text;

namespace SampleApp.Services.Network
{
	public class HttpContent
	{
		public Stream ContentStream { get; private set; }

		public string ContentType { get; private set; }

		public Encoding ContentEncoding { get; private set; }

		public HttpContent(Stream contentStream, string contentType, Encoding encoding)
		{
			ContentStream = contentStream;
			ContentType = contentType;
			ContentEncoding = encoding;
		}

		public HttpContent(string contentText, string contentType, Encoding contentEncoding)
		{
			var bytes = contentEncoding.GetBytes(contentText);

			ContentStream = new MemoryStream(bytes);
			ContentType = contentType;
			ContentEncoding = contentEncoding;
		}
	}
}