﻿using System;
using System.Net;

namespace SampleApp.Services.Network
{
	public class HttpRequestMessage
	{
		public Uri RequestUri { get; set; }

		public HttpMethod Method { get; set; }

		public WebHeaderCollection Headers { get; set; }

		public HttpContent Content { get; set; }

		public HttpRequestMessage(HttpMethod method, Uri requestUri)
		{
			Method = method;
			RequestUri = requestUri;
			Headers = new WebHeaderCollection();
		}
	}
}