﻿using System;

namespace SampleApp.Services.Network
{
	public class HttpMethod
	{
		private static readonly HttpMethod _getMethod;
		private static readonly HttpMethod _postMethod;
		private static readonly HttpMethod _putMethod;
		private static readonly HttpMethod _deleteMethod;
		
		private readonly string _methodName;
		
		public string Name
		{
			get { return _methodName; }
		}

		static HttpMethod()
		{
			_getMethod = new HttpMethod("GET");
			_postMethod = new HttpMethod("POST");
			_putMethod = new HttpMethod("PUT");
			_deleteMethod = new HttpMethod("DELETE");
		}

		private HttpMethod(string methodName)
		{
			if (string.IsNullOrEmpty(methodName))
			{
				throw new ArgumentNullException("methodName");
			}

			_methodName = methodName;
		}

		public static HttpMethod Get
		{
			get { return _getMethod; }
		}

		public static HttpMethod Post
		{
			get { return _postMethod; }
		}

		public static HttpMethod Put
		{
			get { return _putMethod; }
		}

		public static HttpMethod Delete
		{
			get { return _deleteMethod; }
		}

		public override string ToString()
		{
			return _methodName;
		}
	}
}