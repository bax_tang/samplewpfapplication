﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SampleApp.Services.Network
{
	public abstract class ServerGatewayBase
	{
		private const string RequestExecutionError = "An error occured while sending the http web request.";

		private static readonly Action<object> _cancellationHandler;

		private readonly Action<object> _startRequest;
		private readonly AsyncCallback _getResponseCallback;

		static ServerGatewayBase()
		{
			_cancellationHandler = HandleCancellation;
		}

		protected internal ServerGatewayBase()
		{
			_startRequest = StartRequest;
			_getResponseCallback = GetResponseCallback;
		}

		internal Task<ServerResponseRaw> PerformRequest(HttpRequestMessage requestMessage, CancellationToken cancellationToken)
		{
			var completionSource = new TaskCompletionSource<ServerResponseRaw>();

			var requestState = new RequestState(completionSource, requestMessage, cancellationToken);

			try
			{
				HttpWebRequest request = requestState.Request = CreateAndPrepareWebRequest(requestMessage);

				cancellationToken.Register(_cancellationHandler, request);

				Task.Factory.StartNew(_startRequest, requestState, cancellationToken);
			}
			catch (Exception exc)
			{
				HandleAsyncException(requestState, exc);
			}

			return completionSource.Task;
		}

		private HttpWebRequest CreateAndPrepareWebRequest(HttpRequestMessage requestMessage)
		{
			HttpWebRequest request = WebRequest.Create(requestMessage.RequestUri) as HttpWebRequest;
			
			request.Method = requestMessage.Method.Name;

			request.Accept = requestMessage.Headers[HttpRequestHeader.Accept];

			request.ProtocolVersion = new Version(1, 1);
			request.Timeout = -1;

			return request;
		}

		private void HandleAsyncException(RequestState requestState, Exception exc)
		{
			if (requestState.Token.IsCancellationRequested)
			{
				requestState.CompletionSource.TrySetCanceled();
			}
			else if (exc is WebException || exc is IOException)
			{
				requestState.CompletionSource.TrySetException(new HttpRequestException(RequestExecutionError, exc));
			}
			else
			{
				requestState.CompletionSource.TrySetException(exc);
			}
		}

		private static void HandleCancellation(object state)
		{
			(state as HttpWebRequest)?.Abort();
		}

		private void StartRequest(object state)
		{
			RequestState requestState = state as RequestState;
			try
			{
				if (requestState.RequestMessage.Content != null)
				{
					// TODO: write http content to request stream on POST / PUT requests
				}
				else
				{
					requestState.Request.ContentLength = 0L;
					requestState.Request.BeginGetResponse(_getResponseCallback, requestState);
				}
			}
			catch (Exception exc)
			{
				HandleAsyncException(requestState, exc);
			}
		}

		private void GetResponseCallback(IAsyncResult result)
		{
			RequestState requestState = result.AsyncState as RequestState;
			try
			{
				HttpWebResponse response = requestState.Request.EndGetResponse(result) as HttpWebResponse;

				var responseStream = response.GetResponseStream();
				requestState.CompletionSource.TrySetResult(new ServerResponseRaw(response.StatusCode, responseStream, response.Headers));
			}
			catch (Exception exc)
			{
				HandleAsyncException(requestState, exc);
			}
		}
	}
}