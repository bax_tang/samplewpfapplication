﻿using System;
using System.IO;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace SampleApp.Services.Network
{
	public class ResponseParser
	{
		private const int BufferSize = 4096;

		private readonly JsonSerializer _serializer;

		public ResponseParser()
		{
			var settings = new JsonSerializerSettings
			{
				ContractResolver = new DefaultContractResolver(),
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
			};

			_serializer = JsonSerializer.Create(settings);
		}

		public string ParseToString(Stream responseStream)
		{
			string serverMessage = null;

			using (var rawReader = new StreamReader(responseStream, Encoding.UTF8, false, BufferSize))
				serverMessage = rawReader.ReadToEnd();

			return serverMessage;
		}

		public TResponse ParseTo<TResponse>(Stream responseStream)
		{
			using (var rawReader = new StreamReader(responseStream, Encoding.UTF8, false, BufferSize))
			using (var reader = new JsonTextReader(rawReader))
				return _serializer.Deserialize<TResponse>(reader);
		}
	}
}