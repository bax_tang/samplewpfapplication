﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SampleApp.Services.Network
{
	public class ServerGateway : ServerGatewayBase
	{
		public ServerGateway() : base() { }

		internal Task<ServerResponseRaw> PerformGetRequest(Uri requestUri, CancellationToken cancellationToken = default(CancellationToken))
		{
			var message = CreateRequesMessage(requestUri, HttpMethod.Get);

			return PerformRequest(message, cancellationToken);
		}

		private HttpRequestMessage CreateRequesMessage(Uri requestUri, HttpMethod method)
		{
			var requestMessage = new HttpRequestMessage(method, requestUri);

			requestMessage.Headers[HttpRequestHeader.Accept] = "application/json";

			return requestMessage;
		}
	}
}