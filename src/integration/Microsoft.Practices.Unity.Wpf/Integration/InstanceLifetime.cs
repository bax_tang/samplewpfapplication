﻿using System;

namespace Microsoft.Practices.Unity.Wpf
{
	public enum InstanceLifetime
	{
		Single,
		Scoped,
		Transient
	}
}