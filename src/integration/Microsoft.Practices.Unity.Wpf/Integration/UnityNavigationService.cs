﻿using System;
using System.Windows.Mvvm;
using System.Windows.Navigation;

using Microsoft.Practices.Unity;

namespace Microsoft.Practices.Unity.Wpf
{
	internal class UnityNavigationService : INavigationService
	{
		private readonly IUnityContainer _container;
		private readonly NavigationService _navigationService;

		public UnityNavigationService(IUnityContainer container, NavigationService navigationService)
		{
			_container = container;
			_navigationService = navigationService;
		}

		public bool NavigateToViewModel<TViewModel>() where TViewModel : IViewModel
		{
			var viewModel = _container.Resolve<TViewModel>();

			return _navigationService.Navigate(viewModel.AttachedView);
		}
	}
}