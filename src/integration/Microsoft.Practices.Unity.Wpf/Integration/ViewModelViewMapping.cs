﻿using System;
using System.Collections.Generic;
using System.Windows.Mvvm;

namespace Microsoft.Practices.Unity.Wpf
{
	internal class ViewModelViewMapping
	{
		private readonly Dictionary<Type, Type> _mapping;

		public ViewModelViewMapping()
		{
			_mapping = new Dictionary<Type, Type>();
		}

		public void Bind<TViewModel, TView>()
		{
			_mapping[typeof(TViewModel)] = typeof(TView);
		}

		public Type GetViewType(Type viewModelType) => _mapping[viewModelType];
	}
}