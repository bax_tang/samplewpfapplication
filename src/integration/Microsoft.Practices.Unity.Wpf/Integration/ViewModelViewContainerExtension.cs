﻿using System;
using System.Collections.Generic;
using System.Windows.Mvvm;

using Microsoft.Practices.Unity.ObjectBuilder;
using Microsoft.Practices.ObjectBuilder2;

namespace Microsoft.Practices.Unity.Wpf
{
	internal class ViewModelViewContainerExtension : UnityContainerExtension
	{
		public ViewModelViewContainerExtension() { }
		
		protected override void Initialize()
		{
			var mapping = Container.Resolve<ViewModelViewMapping>();

			Context.Strategies.Add(new ViewModelViewAwareStrategy(mapping), UnityBuildStage.Initialization);
		}

		private class ViewModelViewAwareStrategy : BuilderStrategy
		{
			private readonly Type _viewModelType;
			private readonly ViewModelViewMapping _mapping;

			public ViewModelViewAwareStrategy(ViewModelViewMapping mapping)
			{
				_viewModelType = typeof(IViewModel);
				_mapping = mapping;
			}

			public override void PreBuildUp(IBuilderContext context)
			{
				if (_viewModelType.IsAssignableFrom(context.BuildKey.Type))
				{
					var viewType = _mapping.GetViewType(context.BuildKey.Type);
					var viewInstance = context.NewBuildUp(new NamedTypeBuildKey(viewType));

					IViewModel viewModel = context.Existing as IViewModel;

					viewModel.AttachView(viewInstance as IView);
				}
			}

			public override void PreTearDown(IBuilderContext context)
			{
			}
		}
	}
}