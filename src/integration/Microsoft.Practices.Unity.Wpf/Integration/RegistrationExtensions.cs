﻿using System;
using System.Windows.Mvvm;
using System.Windows.Navigation;

namespace Microsoft.Practices.Unity.Wpf
{
	public static class RegistrationExtensions
	{
		public static void RegisterNavigationService(this IUnityContainer container, NavigationService navigationService)
		{
			container.RegisterInstance(new ViewModelViewMapping());

			container.AddExtension(new ViewModelViewContainerExtension());
			
			container.RegisterInstance<INavigationService>(new UnityNavigationService(container, navigationService));
		}

		public static void RegisterType<T>(this IUnityContainer container, InstanceLifetime instanceLifetime)
		{
			container.RegisterType<T>(GetLifetimeManager(instanceLifetime));
		}

		public static void Bind<TViewModel, TView>(this IUnityContainer container,
			InstanceLifetime viewModelLifetime = InstanceLifetime.Transient,
			InstanceLifetime viewLifetime = InstanceLifetime.Transient)
		{
			container.RegisterType<TViewModel>(GetLifetimeManager(viewModelLifetime));
			container.RegisterType<TView>(GetLifetimeManager(viewLifetime));

			container.Resolve<ViewModelViewMapping>().Bind<TViewModel, TView>();
		}

		private static LifetimeManager GetLifetimeManager(InstanceLifetime instanceLifetime)
		{
			switch (instanceLifetime)
			{
				case InstanceLifetime.Single:
					return new ContainerControlledLifetimeManager();
				case InstanceLifetime.Transient:
					return new TransientLifetimeManager();
				case InstanceLifetime.Scoped:
				default:
					return new PerThreadLifetimeManager();
			}
		}
	}
}