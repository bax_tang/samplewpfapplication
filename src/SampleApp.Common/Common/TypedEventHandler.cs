﻿using System;

namespace SampleApp.Common
{
	public delegate void TypedEventHandler<TSender, TArgs>(TSender sender, TArgs args);
}