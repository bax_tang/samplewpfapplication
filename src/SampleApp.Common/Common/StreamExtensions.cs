﻿using System;
using System.IO;

namespace SampleApp.Common
{
	public static class StreamExtensions
	{
		public static void CopyTo(this Stream source, Stream destination, int bufferSize)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (destination == null)
			{
				throw new ArgumentNullException("destination");
			}
			if (!source.CanRead)
			{
				throw new InvalidOperationException("Source stream don't support read operation.");
			}
			if (!destination.CanWrite)
			{
				throw new InvalidOperationException("Destination stream don't support write operation.");
			}
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize");
			}

			InternalCopyTo(source, destination, bufferSize);
		}

		private static void InternalCopyTo(Stream source, Stream destination, int bufferSize)
		{
			byte[] buffer = new byte[bufferSize];
			int count;

			do
			{
				count = source.Read(buffer, 0, bufferSize);
				if (count != 0)
				{
					destination.Write(buffer, 0, count);
				}
			}
			while (count != 0);
		}
	}
}