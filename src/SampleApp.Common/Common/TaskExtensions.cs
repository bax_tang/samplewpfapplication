﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace SampleApp.Common
{
	public static class TaskExtensions
	{
		public static void DontAwaitButCheckExceptions(this Task task)
		{
			task.ContinueWith(HandleFaultedTask, TaskContinuationOptions.OnlyOnFaulted);
		}

		private static void HandleFaultedTask(Task faultedTask)
		{
			if (faultedTask.Exception != null)
			{
				var baseException = faultedTask.Exception.GetBaseException();

				Trace.WriteLine(string.Format("Unhandled task exception occured: {0}", baseException));

#if DEBUG
				Debugger.Break();
#endif
			}
		}
	}
}