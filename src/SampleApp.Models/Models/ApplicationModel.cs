﻿using System;
using System.Threading;
using System.Threading.Tasks;

using SampleApp.Common;
using SampleApp.Domain;
using SampleApp.Services.Network;

namespace SampleApp.Models
{
	public class ApplicationModel
	{
		private readonly ApiService _apiService;

		public event Action<IPInformation> IpInformationLoaded;

		public event Action<Exception, string> IpInformationFailed;

		public ApplicationModel(ApiService apiService)
		{
			_apiService = apiService;
		}

		public async Task StartGetIpInformationAsync()
		{
			var response = await _apiService.GetIpInformationAsync();
			if (response.IsSuccess())
			{
				IpInformationLoaded?.Invoke(response.ResponseResult);
			}
			else
			{
				IpInformationFailed?.Invoke(response.Error, response.ServerMessage);
			}
		}
	}
}