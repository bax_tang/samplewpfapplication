﻿using System;

using Microsoft.Practices.Unity;

using SampleApp.Services.Network;

namespace SampleApp.Models.Tests
{
	internal static class ContainerFactory
	{
		public static IUnityContainer CreateContainer()
		{
			IUnityContainer container = new UnityContainer();

			container.RegisterType<ResponseParser>();
			container.RegisterType<ServerGateway>();
			container.RegisterType<ApiService>();
			container.RegisterType<ApplicationModel>();

			return container;
		}
	}
}