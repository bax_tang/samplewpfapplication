﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Practices.Unity;

using Newtonsoft.Json;
using NUnit.Framework;

using SampleApp.Common;
using SampleApp.Domain;
using SampleApp.Services.Network;

namespace SampleApp.Models.Tests
{
	[TestFixture]
	public class ApplicationModelTests
	{
		#region Test methods

		[Test]
		public void IsIpInformationLoadsSuccessfully()
		{
			var resetEvent = new AutoResetEvent(false);

			var model = _container.Resolve<ApplicationModel>();

			model.IpInformationLoaded += (info) =>
			{
				Console.WriteLine(JsonConvert.SerializeObject(info, Formatting.Indented));
				resetEvent.Set();
			};

			model.IpInformationFailed += (e, msg) =>
			{
				Console.WriteLine("{0}: {1}", msg, e);
				resetEvent.Set();
			};

			model.StartGetIpInformationAsync().DontAwaitButCheckExceptions();

			resetEvent.WaitOne();
		}
		#endregion

		private IUnityContainer _container;

		[SetUp]
		public void Initialize()
		{
			_container = ContainerFactory.CreateContainer();
		}
	}
}